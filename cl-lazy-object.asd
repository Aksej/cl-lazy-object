;;;; lazy-object.asd

(asdf:defsystem #:cl-lazy-object
  :description "Objects with lazy slots"
  :author "Thomas Bartscher <thomas-bartscher@weltraumschlangen.de>"
  :license "EUPL V1.1"
  :version "1.0"
  :components
  ((:file "package")
   (:module
    "source"
    :depends-on ("package")
    :components
    ((:file "lazy-object")
     ))
   ))
