(in-package #:cl-lazy-object)

(defparameter cl-lazy-object-symbols::*counter* 0)

(defun mk-lazy-symbol (&optional (name "lazy-symbol"))
  (incf cl-lazy-object-symbols::*counter*)
  (intern (format nil "~A~D"
                  name cl-lazy-object-symbols::*counter*)
          '#:cl-lazy-object-symbols))

(defun car-or-x (obj)
  (if (consp obj)
      (car obj)
      obj))

(defmacro deflazytype (name (&rest superclasses) &body slots)
  (let* ((slot-names (mapcar #'car-or-x
                             slots))
         (lazy-slots (mapcar (lambda (slot)
                               (mk-lazy-symbol slot))
                             slot-names))
         (slot-types (mapcar (lambda (slot)
                               (if (consp slot)
                                   (getf (rest slot)
                                         :type t)
                                   t))
                             slots)))
    `(progn
       (defclass ,name ,superclasses
         (,@(mapcar (lambda (lslot)
                      `(,lslot :initarg ,lslot
                               :type (or function null)))
                    lazy-slots)
          ,@(mapcar (lambda (slot-name slot-type)
                      `(,slot-name :type ,slot-type))
                    slot-names slot-types)))
       (defmacro ,name (,@lazy-slots)
         `(make-instance
           ',',name
           ,,@(loop for lazy-slot in lazy-slots
                 for slot-type in slot-types
                 append
                   `('',lazy-slot `(lambda ()
                                     (let ((result ,,lazy-slot))
                                       (assert (typep result
                                                      ',',slot-type))
                                       result))))))
       ,@(mapcar (lambda (slot lazy-slot)
                   `(defmethod ,slot ((object ,name))
                      (let ((thunk (slot-value object ',lazy-slot)))
                        (when thunk
                          (setf (slot-value object ',slot)
                                (funcall thunk)
                                (slot-value object ',lazy-slot)
                                nil))
                        (slot-value object ',slot))))
                 slot-names lazy-slots))))
